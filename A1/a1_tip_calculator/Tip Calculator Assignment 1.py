#!/usr/bin/env python3

 

print ("Tip Calculator\n")

print ("\nProgram Requirements: \n"

+"1. Must use float data type for user input (except, \"Party Number\").\n"

+"2. Must round calculations to two decimal places.\n"

+"3. Must format currency with dollar sign, and two decimal places.")

 

 

print("\nUser Input:")

meal_cost = float(input("Cost of meal: "))

tax_percent = float(input("Tax percent: "))

tip_percent = float (input("tip_percent: "))

people_num = int (input("Party number: "))

 

# Calculate tax, tip and total amount

tax_amount = round(meal_cost * (tax_percent / 100),2) # convert to percentage

due_amount = round(meal_cost + tax_amount, 2)

tip_amount = round((due_amount) * (tip_percent / 100),2) #percentage of cost + tax

total = round(meal_cost + tax_amount + tip_amount, 2)

split = round(total / people_num, 2)

 

#display results

#Formatting:

print("\nProgram Output:")

print ("Subtotal:\t $" "%.2f" % meal_cost )

print ("Tax:\t\t $" "%.2f" % tax_amount )

print ("Amount Due:\t $" "%.2f" % due_amount )

print ("Gratuity:\t $" "%.2f" % tip_amount )

print ("Total:\t\t $" "%.2f" % total )

print( "Split (" + "%.0f" % people_num  + ("):\t $") + ("%.2f" % split ) )