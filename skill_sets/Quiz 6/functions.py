#SkillSet_6
#!/usr/bin/env python3
def get_requirements():

    print ("Python Looping Structures\n")
print ("\nProgram Requirements: \n"
    +"1. Use Print while loop \n"
    +"2. Print for loops using range() function, and implicit and explicit lists.\n"
    +"3. Use break and continue statements. \n" 
    +"4. Replicate display below")

def looping_python_structure():

    print("1.while loop:")
    i = 1
    while i < 4:

        print(i)

        i += 1

 

    print("\n2. For loop using range() function with 1 arg:")
    for i in range(4):
        print(i)

 

    print("\n3. For loop using range() function with two arg:")

    for i in range(1,4):
        print(i)

 

    print("\n4. #For loop using range() function with three arg: (interval 2):")

    for i in range(1,4,2):
        print(i)

 

    print("\n5. #For loop using range() function with three arg: (negative interval):")

    for i in range(3,0,-2):
        print(i)

 

    print("\n6. #For loop using (implicit) list:")

    for num in [1,2,3]:

        print(num)

       

 

    print("\n7. for loop iterating through (explicit) string list:")

    stateList = ['Michigan', 'Alabama', 'Florida']

    for state in stateList:

        print(state)

       

    print("\n8. for loop using break statment (stops loop):")

    stateList = ['Michigan', 'Alabama', 'Florida']

    for state in stateList:


        if state == 'Florida':

            break
        print(state)    
       

        

    print("\n9. for loop using continue statment (stops and continues with next):")

    stateList = ["Michigan", "Alabama", "Florida"]

    for state in stateList:

        if state == "Alabama":

            continue

        print(state)

 

    print("\n10. print list length:")

    stateList = ["Michigan", "Alabama", "Florida"]

    print (len(stateList))   

