#!/usr/bin/env python3

print ("Square feet to\n")

print ("\nProgram Requirements: \n"

    +"1. Research: number of square feet to acre of land. \n"

    +"2. Must use float data type for user input and calculation\n"

    +"3. Format and round conversion to two decimal places")


print("\nInput:")

square_feet = float(input("Enter square feet : "))


value = 43560

acres = round(square_feet / value, 2)

print("\nProgram Output:")

print ( "{:,.2f}".format(square_feet), "squart feet = " , "%.2f" % acres + " acres" )