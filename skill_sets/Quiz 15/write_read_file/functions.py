#SkillSet_15
def get_requirements():
    # constants to represent base hours, overtime and holiday rates
    print ("\nFile, Write, Read\n")
    print ("Program Requirements: \n"
        +"1. Create write_read_file subdirectory with two files: main.py and functions.py.\n"
        +"2. Use President Abraham Lincoln's Gettysburg Address: Full Text\n"
        +"3. Write address to file.\n"
        +"4. Read address from same file.\n" 
        +"5. Create Python Docstrings for functions in functions.py.\n" 
        +"6. Display Python Docstrings for each function in functions.py file.\n"
        +"7. Display full file path \n"  
        +"7. Replicate display below\n")

def write_read_file():
#two functions
    file_write()
    file_read() 


def file_write():

    f = open("G_Letter.text", "w")
    f.write("President Abraham Lincoln's Gettysburg Address:\n"
    + "Four score and seven years ago our fathers brought forth, upon this continent, a new nation, conceived in liberty, and dedicated to the\n"
    + "proposition that all men are created equal.\n"
    + "\nNow we are engaged in a great civil war, testing whether that nation, or any nation so conceived, and so dedicated, can long endure.\n" 
    + "We are met on a great battle field of that war. We come to dedicate a portion of it, as a final resting place for those who \n" 
    + "here gave thier lives that nation might live. It is altogether fitting and proper that we should do this \n"
    + "\nBut, in a larger sense, we can not dedicate we can not consecrate we can not hallow, this ground. The brave men, living and dead,\n" 
    + "who struggled here, have consecrated it, far above our poor power to add or detract. The world will little note, nor long remember \n" 
    + "what we say here; while it can never forget what they did here. It is rather for us, the living, to be dedicated here to the unfinished \n" 
    + "work which they who here have this far so nobly advanced. It is rather for us to be here dedicated to the great task remaining\n"
    + "before us -- that from these honored dead we take increated devotion to that cause for which they gave the last full measure of devotion\n"
    + "that we here highly resolve that these dead shall not have died in vain -- that this nationm under God, shall have a new birth\n"
    + "of freedom -- and that government of the people, by the people, for the people, shall not perish from the earth.\n"
    + "\nAbraham Lincoln\n"
    + "November 19, 1863\n")
    f.close()


def file_read():
    f = open("G_Letter.text", "r" )
    print(f.read())
    print("Full file Path")
    print(r"c:\Users\Slyfire18\Desktop\FSU Terms\FSU Fall 2019\skill_sets\Quiz 15\write_read_file\G_Letter.text")