#SkillSet_3
def get_requirements():
    # constants to represent base hours, overtime and holiday rates
    print ("IT/ICT Student Percentage\n")

print ("\nProgram Requirements: \n"
    +"1. Find number of IT/ICT students in class  \n"
    +"2. Calcualte IT/ICT Student Percentage \n"
    +"3. Must use float data type (to facilitate right-alignment\n" 
    +"4. Format, right-align numbers, and round to two decimal places")

def calculate_student_percentage():

    IT = 0.0
    ICT = 0.0

    print("\nInput:")
    IT = float(input("Enter number of IT students  : "))
    ICT = float(input("Enter number of ICT students : "))

    total = IT + ICT
    percent_ict = ICT / total
    percent_it = IT / total

    print("\nOutput:")
    print ("{0:17} {1:>5.2f}".format ("Total Students:", total))
    print ("{0:17}".format ("IT Students:", percent_it))
    print ("{0:17} {1:>5.2%}".format ("ICT Students:", percent_ict))
   