#SkillSet_7
#!/usr/bin/env python3
def get_requirements():

    print ("Python Lists\n")
print ("\nProgram Requirements: \n"
    +"1. List (Python data structure): mutable, ordered sequence of elements. \n"
    +"2. List are mutable/changeable--that is, can insert, update, delete.\n"
    +"3. Create list - using square brackets [list]: my_list = [""cherries"", ""apples"", ""bananas"", ""oranges""] \n" 
    +"4. Create a program that mirrors that following IPO (input/process/outpout) format. \n"
    +"Note: user enters number of requsted list elements, dynamically rendered below (that is, number of elements can change each run")

def python_data_structure():

    my_lists = []

    print("Input: ")

    list_range = int(input("Enter number of list elements: "))

    x = 0

    for x in range(0, list_range):

        x += 1
        element = str(input("\nPlease enter list element "+str(x)+":"))
        my_lists.append(element)

 

    print("\nOutput:")

    print("Print my_list:")

    print(my_lists)

 

    element1 = str(input("Please enter list element: "))

    insert = int(input("Please enter list *index* position (note: must convert to int): "))

 

    my_lists.insert(insert,element1)

    print("\nInsert element into specific position in my my_list")

    print(my_lists)

 

    print("\nCount number of elements in List:")

    print(len(my_lists))

 

    print("\nSort elements in list alphabetically: ")

    my_lists.sort()

    print(my_lists)

 

    print("\nReverse List:")

    my_lists.sort(reverse=True)

    print(my_lists)

 

    print("\nRemove last list element: ")

    my_lists.pop()

    print(my_lists)

 

    print("\nDelete second element from last list by *index* (note: 1=2nd element): ")

    my_lists.pop(-3)

    print(my_lists)

 

    print("\nDelete element from list by *value* (cherries): ")

    my_lists.remove('cherries')

    print(my_lists)

 

    print("\nDelete all element from list: ")

    my_lists.clear()

    print(my_lists)