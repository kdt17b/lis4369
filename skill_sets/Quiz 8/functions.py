#SkillSet_8
#!/usr/bin/env python3
def get_requirements():

    print ("Python Tuples\n")
    print ("\nProgram Requirements: \n"
    +"1. Tuples (Python data structure): *immutable* (cannont be changed!), ordered sequence of elements.\n"
    +"2. Tuples are immutables/unchangeable--that is, cannont insert, update delete.\n"
    +"Note: can reassign or delete an *entire* tuple--but, *not* individual items or slices.\n"
    +"3. Create tuple using parentheses (tuple): my_tuple1 = (""cherries"", ""apples"", ""bananas"", ""oranges"")\n" 
    +"4. Create tuple (packing), that is, *without* using parentheses (aka tuple ""packing""): my_tuple2 = 1,2,""three"", ""four"" \n"
    +"5. Python tuple (unpacking), that is, assign values from tuple to sequence of variables: fruit1,fruit2,fruit3,fruit4 = my_tuple1\n"
    +"6. Create a program that mirrors that following IPO (input,process/output) format.")

def python_data_structure():

    print("\nInput: Hard coded--no user input")

    print("Output:")
    print("\nPrint my_tuple1: ")
    my_tuple1 = ("cherries","apples","bananas","oranges")

    print(my_tuple1) 

    print("\nPrint my_tuple2: ")
    my_tuple2 = ("1","2","three","four")
    print(my_tuple2)

    print("\nPrint my_tuple1 unpacking: ")
    (cherries,apples,bananas,oranges) = my_tuple1
    print(cherries,apples,bananas,oranges)

    print("\nPrint third element in my_tuple2: ")
    print(my_tuple2[2])
  
    print("\nPrint ""slice"" of my_tuple1 (second and tird elements): ")
    print(my_tuple1[1:3])

    print("\nReassign my_tuple2 using parentheses.")
    print("\nPrint my_tuple2:")
    my_tuple2 = (1,2,3,4)
    print(my_tuple2)

    print("\nReassign my_tuple2 using ""packing"" method (no parentheses)")
    print("\nPrint my_tuple2:")
    my_tuple2 = (5,6,7,8)
    print(my_tuple2)

    print("\nPrint number of elements in my_tuple1:")
    print(len(my_tuple1))
    
    print("\nPrint type of my_tuple1:")
    print(type(my_tuple1))

    print("\nDelete my_tuple1:")
    del(my_tuple1)