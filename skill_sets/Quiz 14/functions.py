#SkillSet_14
def get_requirements():
    # constants to represent base hours, overtime and holiday rates
    print ("Python Selection Structures\n")

print ("\nProgram Requirements: \n"
    +"1. Program calculates two numbers, and rounds to two decimal places.\n"
    +"2. Prompt user for two numbers, and a suitable operator\n"
    +"3. Use Python error handling to validate data.\n"
    +"4. Test for correct arithmetic operator.\n" 
    +"5. Division by zero not permitted.\n" 
    +"6. Note: Program loops until correct input entered - numbers and arithmetic operator.\n"  
    +"7. Replicate display below")

def calculate_python_calculator():

    import math

    num1 = 0
    num2 = 0.0
    test = True
    test2 = True
    print("\nInput:")

    while (test == True):
        num1 = input("\nEnter num1: ")
        try:
            num1 = float(num1)
            num2 = input("\nEnter num2: ")
            try:    
                num2 = float(num2)
                test = False
            except ValueError:
                print("Not Valid Number!")
                num2 = input("\nEnter num2: ")
                while num2.isnumeric() == False:
                    print("Not Valid Number!")
                    num2 = (float(input("\nEnter num2: ")))
                else:
                    num2 = float(num2)
                    test = False
                           
        except ValueError:
            print("Not Valid Number!") 
            continue    

    print("\nSuitable Operators: +, -, *, /,// (integer division), % (modulo operator), ** (power)")
    
    while (test2 == True):
        selection = (input("\nEnter Operator : "))
    
        if selection == '+':
            #print (num1+num2)
            print("{0:.2f}".format(num1+num2))
            test2 = False

        elif selection == '-':
            #print (num1-num2)
            print("{0:.2f}".format(num1-num2))
            test2 = False

        elif selection == '*':
            #print (num1*num2)
            print("{0:.2f}".format(num1*num2))
            test2 = False

        elif selection == '/':
            if num2 == 0:
                print("Cannot Divide by zero!")
                num2 = (float(input("\nEnter num2: ")))
            else:
                #print (num1/num2)
                print("{0:.2f}".format(num1/num2))
                test2 = False    
                    
        elif selection == '//':
            if num2 == 0:
                print("Cannot Divide by zero!")
                num2 = (float(input("\nEnter num2: ")))
            else:
                #print (num1//num2)
                print("{0:.2f}".format(num1//num2))
                test2 = False   

        elif selection == '%':
            #print (num1%num2)
            #print("{0:.2f}".format(num1%num2))
            #test2 = False
            if num2 == 0:
                print("Cannot Divide by zero!")
                num2 = (float(input("\nEnter num2: ")))
            else:
                #print (num1//num2)
                print("{0:.2f}".format(num1%num2))
                test2 = False   

        elif selection == '**':
            #print (num1**num2)
            #print (pow(num1,num2)) 
            print("{0:.2f}".format(num1**num2))
            print("{0:.2f}".format(pow(num1,num2)))
            test2 = False

        elif selection != ['+','-','*','/','//','%','**']: 
            print ("\nIncorrect Operator") 
            continue                          
    print("\nThank you for using our Math Calculator!\n")        
