#SkillSet_11
#!/usr/bin/env python3
def get_requirements():

    print ("\nPseudo-Random Number Generator\n")
    print ("\nProgram Requirements: \n"
    +"1. Get User beginning and ending integer values, and store in two variablesn"
    +"2. Display 10 pseudo-random numbers between, and including, above values.\n"
    +"3. Must use integer data types\n" 
    +"4. Example 1: Using range() and randint() functions\n"
    +"5. Example 2: Usng a list with range() and shuffle() functions.\n")

def python_random_number_structure():

    import random

    print("\nInput:")
    start = int(input("\nEnter beginning value::"))
    end = int(input("\nEnter Ending value::"))  

    print("\nOutput:")
    #print("Example 1: Using range() and randint() functions:")
    #x = [random.randint(bvalue,evalue) for i in range(10)]
    #print(x)
    for count in range(10):
        print(random.randint(start, end), sep=",", end=" ")

    print("\nExample 2: Using range() and Shuffle() functions:")
    #numberlist = [x]
    #[random.shuffle(numberlist) for i in range(10)]
    #print(numberlist)
    #print([random.shuffle(x) for i in range(10)])
    
    r = list(range(start,end +1))
    random.shuffle(r)
    for i in r:
            print(i, sep=",",end=" ")
    print()