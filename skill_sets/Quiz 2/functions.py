#SkillSet_2
def get_requirements():
    # constants to represent base hours, overtime and holiday rates
    print ("Miles per Gallon\n")

print ("\nProgram Requirements: \n"
    +"1. Convert MPG \n"
    +"2. Must use float data type for user input and calculation\n"
    +"3. Format and round conversion to two decimal places")

def calculate_miles_per_gallon():

    miles = 0.0
    gallons = 0.0
    mpg = 0.0

    print("\nInput:")
    miles = float(input("Enter miles driven : "))
    gallons = float(input("Enter gallons of fuel used : "))

    mpg = miles / gallons

    print("\nOutput:")
    print ( "%.2f" % miles, "miles driven and " , "%.2f" % gallons + " gallons used =" , "%.2f" % mpg , "mpg " )    