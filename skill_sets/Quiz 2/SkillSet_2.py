print ("Miles per Gallon\n")

print ("\nProgram Requirements: \n"

    +"1. Convert MPG \n"

    +"2. Must use float data type for user input and calculation\n"

    +"3. Format and round conversion to two decimal places")


print("\nInput:")

miles_driven = float(input("Enter miles driven : "))
gallons = float(input("Enter gallons of fuel used : "))



mpg = round(miles_driven / gallons)

print("\nOutput:")

print ( "%.2f" % miles_driven, "miles driven and " , "%.2f" % gallons + " gallons used =" , "%.2f" % mpg , "mpg " )