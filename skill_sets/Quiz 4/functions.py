#SkillSet_3
def get_requirements():
    # constants to represent base hours, overtime and holiday rates
    print ("Calorie Percentage\n")

print ("\nProgram Requirements: \n"
    +"1. Find calories per grams of fat, carbs, and protein \n"
    +"2. Calculate Percentages \n"
    +"3. Must use float data type \n" 
    +"4. Format, right-align numbers, and round to two decimal places")

def calculate_calorie_percentage():

    fat = 0.0
    carb = 0.0
    protein = 0.0

    print("\nInput:")
    fat = float(input("Enter total fat grams : "))
    carb = float(input("Enter total carb grams :  "))
    protein = float(input("Enter total protein grams :  "))

    fat_total_calories = fat * 9
    carb_total_calories = carb * 4
    protein_total_calories = protein * 4 

    total_calories = fat_total_calories + carb_total_calories + protein_total_calories

    fat_percent = fat_total_calories / total_calories * 100
    carb_percent = carb_total_calories / total_calories * 100
    protein_percent = protein_total_calories / total_calories * 100


    print("\nOutput:")
    print ("{0:8} {1:>11} {2:>11}".format ("Type","Calories","Percentage"))

    print ("{0:12} {1:>4.2f} {2:>10.2f}%".format ("Fat:",fat_total_calories,fat_percent))
    print ("{0:12} {1:>4.2f} {2:>10.2f}%".format ("Carbs:",carb_total_calories,carb_percent))
    print ("{0:12} {1:>4.2f} {2:>10.2f}%".format ("Protein:",protein_total_calories,protein_percent))