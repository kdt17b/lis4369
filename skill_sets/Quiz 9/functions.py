#SkillSet_9
#!/usr/bin/env python3
def get_requirements():

    print ("\nPython Sets - Like mathematical sets!\n")
    print ("\nProgram Requirements: \n"
    +"1. Sets (Python data structure): mutable, heterogeneous, unordered sequence of elements, *but* cannont hold duplicate values.\n"
    +"2. Sets are mutable/changeable--that is, can inset, update, delete.\n"
    +"3. While sets are mutable/changeable, they *cannot* contain other mutable items like list, set, or dictionary--\n" 
    +"\that is, elemnts contained in set must be immutable. \n"
    +"4. Also, since sets are unordered, cannot use indexing(or, slicing) to access, update, or delete elements. \n"
    +"5. Two methods to create sets:\n"
    +"\t a. Create set using curly brackets {set}: my_set = {1, 3.14, 2.0, 'four', 'Five'} \n"
    +"\t b. Create set using set() function: my_set =(<iterable>) \n"
    +"\t Examples: \n"
    +"\t my_set1 = set([1, 3.14, 2.0, 'four', 'Five']) \n "
    +"\t my_set2 = set(([1, 3.14, 2.0, 'four', 'Five')) \n "
    +"5. Note: An ""iterable"" is *any* object, which can be iterated over-- that is, lists, tuples, or seven strings.\n "
    +"6. Create a program that mirrors the following IPO (input/process/output) format.\n ")

def python_sets_structure():

    print("\nInput: Hard coded--no user input. See three examples above")
    print("***Note***: All three sets below print as ""sets"" (i.e., curly brackets), *regardless* of how they were created!)")

    print("\nPrint my_set created using curly brackets:")
    my_set = {1, 2.0, 3.14, 'four', 'Five'}
    print(my_set)

    print("\nPrint type of my_set:")
    print(type(my_set))

    print("\nPrint my_set1 created using set() function with list: ")
    my_set1 = set({1, 2.0, 3.14, 'four', 'five'})
    print(my_set1)

    print("\nPrint type of my_set1:")
    print(type(my_set1))

    print("\nPrint my_set2 created using set() function with tuple: ")
    my_set2 = set((1,2.0,3.14,'four','Five'))
    print(my_set2)

    print("\nPrint type of my_set2:")
    print(type(my_set2))

    print("\nLength of my_set:")
    print(len(my_set))
    
    print("\nDiscard 'four:")
    my_set.remove('four')
    print(my_set)

    print("\nRemove 'five:")
    my_set.remove('Five')
    print(my_set)

    print("\nLength of my_set:")
    print(len(my_set))

    print("\nAdd element to set (4) using add() method:")
    my_set.add(4)
    print(my_set)

    print("\nLength of my_set:")
    print(len(my_set))

    print("\nDisplay minimun number:")
    print(min(my_set))

    print("\nDisplay maximun number:")
    print(max(my_set))

    print("\nDisplay maximun number:")
    print(sum(my_set))

    print("\nDelete all set elements:")
    #my_set.remove(1)
    #my_set.remove(2.0)
    #my_set.remove(3.14)
    #my_set.remove(4)
    my_set.clear()
    print(my_set)

    print("\nLength of my_set:")
    print(len(my_set))