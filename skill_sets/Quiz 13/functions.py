#SkillSet_13
#!/usr/bin/env python3
def get_requirements():

 print("Sphere Volume Calculator \n")

 print("\nProgram Requirements: \n"
 +"1. Program calculates sphere volume in liquid U.S. gallons from user-enterd diameter value in inches\n"
 +"and rounds to two decimal places.\n"
 +"2. Must use Python's *built-in* PI and pow() capabilities.\n"
 +"3. Program checks for non-integers and non-numerics values.\n"
 +"4. Program continues to prompt for user entry until no longer requested, prompt accepts upper of lower case letters\n")


def Sphere_Volume_Calculator():   
    import math 

    volume = 0
    choice = ''
    number = ''

    print ("input")

    choice = input ("\nDo you want to calculate a sphere volume (y or n)?").lower()

    print("Output")

    while (choice[0]=='y'):
        number = input("\nPlease enter diameter in inches: ")
        
        if number.isnumeric():
            radius = int(number) / 2 
            volume = (4/3)* math.pi * (pow(radius,3))
            total = volume / 231
            print("Sphere volume: " "{0:.2f}" " liquid U.S. gallons ".format(total))
            choice = input(
                "\nDo you want to calculate another sphere volume? ").lower()
        else:
            print("Not valid interger!")
            number = input("\nPlease enter diameter in inches: ")
            while number.isnumeric() == False:
                print("Not valid interger!")
                break
                #number = input("\nPlease enter diameter in inches: ")

    print("\nThank you for using our Sphere Volume Calculator!")        