#LIS 4369
> **NOTE:** This README.md file should be placed at the **root of each of your repos directories.**
>
>Also, this file **must** use Markdown syntax, and provide project documentation as per below--otherwise, points **will** be deducted.
>

# LIS4369 - Extensible Enterprise Solutions

## Kenneth D Turner II

### Assignment 4 Requirements:

*Five Parts:*
    Data Analysis 2

    Program Requirements:
    1. Run demo.py
    2. If errors, more than likely missing installations
    3. Test Python Package Installer: pip freeze
    4. Research how to do the following installations:
     a. pandas (only if missing)
     b. pandas-datareader (only if missing)
     c. matplotlib (only if missing)
    5. Create at least three functions that are called by the program:
     a. main(): calls two other functions:
     b. get_requirements(): displays the program requirements
     c. data_analysis_2(): displays the following data

#### README.md file should include the following items:

* Assignment requirements, as per A4
* Screenshot of application running

#### Assignment Screenshots:

*Screenshot of Data Analysis 2 application running (IDLE)

![IDLE Screenshot](img/Data_Analysis1_IDLE.png)
![IDLE Screenshot](img/Data_Analysis2_IDLE.png)
![IDLE Screenshot](img/Data_Analysis3_IDLE.png)
![IDLE Screenshot](img/Data_Analysis4_IDLE.png)
![IDLE Screenshot](img/Data_Analysis5_IDLE.png)
![IDLE Screenshot](img/Data_Analysis6_IDLE.png)
![IDLE Screenshot](img/Data_Analysis7_IDLE.png)
![IDLE Screenshot](img/Data_Analysis8_IDLE.png)
![IDLE Screenshot](img/Data_Analysis9_IDLE.png)

*Screenshot of Data Analysis 2 running (Visual Studio Code)

![Visual Studio Code](img/Data_Analysis1.png)
![Visual Studio Code](img/Data_Analysis2.png)
![Visual Studio Code](img/Data_Analysis3.png)
![Visual Studio Code](img/Data_Analysis4.png)
![Visual Studio Code](img/Data_Analysis5.png)
![Visual Studio Code](img/Data_Analysis6.png)
![Visual Studio Code](img/Data_Analysis7.png)
![Visual Studio Code](img/Data_Analysis8.png)
![Visual Studio Code](img/Data_Analysis9.png)
![Visual Studio Code](img/Data_Analysis10.png)
![Visual Studio Code](img/Data_Analysis_Graph.png)

*Please be advised that the above Graph is split because multiple entires between 1000 - 1250 have no ages to display.  

#### Tutorial Links:

*Bitbucket Tutorial - Station Locations:*
[A1 Bitbucket Station Locations Tutorial Link](https://bitbucket.org/kdt17b/bitbucketstationlocations/ "Bitbucket Station Locations")

*Tutorial: Request to update a teammate's repository:*
[A1 My Team Quotes Tutorial Link](https://bitbucket.org/kdt17b/myteamquotes/ "My Team Quotes Tutorial")
