#LIS 4369
> **NOTE:** This README.md file should be placed at the **root of each of your repos directories.**
>
>Also, this file **must** use Markdown syntax, and provide project documentation as per below--otherwise, points **will** be deducted.
>

# LIS4369 - Extensible Enterprise Solutions

## Kenneth D Turner II

### Project 2 Requirements:

*Three Parts:*

### Assignment Requirements ***

    #1.  Use Assignment 5 screenshots and R Manual to backward-engineer the following requirements:
    #2.  Resources:a. R Manual: https://cran.r-project.org/doc/manuals/r-release/R-lang.pdfb. R for Data Science: https://r4ds.had.co.nz/
    #3.  Use Motor Trend Car Road Tests data:
        #a. Research the data! https://stat.ethz.ch/R-manual/R-devel/library/datasets/html/mtcars.html
        #b. url = "http://vincentarelbundock.github.io/Rdatasets/csv/datasets/mtcars.csv"
    #Note: Use variable "mtcars" to read file into. 
    #(See Assignment 5 for reading .csv files.)


#### README.md file should include the following items:

* Assignment requirements, as per project 1
* Screenshot of application running

#### Assignment Screenshots:


*Screenshot of Graph Plot running (Rstudio)

![ RStudio Code](img/Rplot_P2.png)
![ RStudio Code](img/Rplot01_P2.png)


#### Tutorial Links:

*Bitbucket Tutorial - Station Locations:*
[A1 Bitbucket Station Locations Tutorial Link](https://bitbucket.org/kdt17b/bitbucketstationlocations/ "Bitbucket Station Locations")

*Tutorial: Request to update a teammate's repository:*
[A1 My Team Quotes Tutorial Link](https://bitbucket.org/kdt17b/myteamquotes/ "My Team Quotes Tutorial")
