# LIS 4369

## Kenneth Turner 

 [A1 README.md](A1/README.md "A1 File")

   - tip_calculator application

   - Assignment#1: The below instructions are to install the needed software for Python, and then create an tip_calculator application

    -   Install Python
    -   Install R
    -   Install R Studio
    -   Install Visual Studio Code
    -   Create a1_tip_calculator application
    -   Provide screenshots of installations
    -   Create Bitbucket repo
    -   Complete Bitbucket tutorial (bitbucketstationlocations)
    -   Provide git command descriptions

 [A2 README.md](A2/README.md "A2 File")

- Payroll Calculator

- This Code will create a Payroll Calculator using Python Software:

- Program Requirements
   - Must Use float data type for user input.
   - Overtime rate: 1.5 times hourly rate (hours over 40).
   - Holiday rate: 2.0 times hourly rate (all holiday hours)
   - Must format currency with dollar sign, and round to two decimal places.
   - Create at least three functions that are called by the program:
   -  a. main(): calls at least two other functions.
   -  b. get_requirements(): displays the program requirements
   -  c. calculate_payroll(): calculates an individual one-week paycheck

 [A3 README.md](A3/README.md "A3 File")

- Painting Estimator

- This Code will create a PPainting Estimator using Python Software:

- Program Requirements:
   - Calculate home interior paint cost (w/o primer).
   - Must use float data types.\n"
   - Must use SQFT_PER_GALLON constant (350).
   - Must use iteration structure (aka ""loop"")
   - Create at least five functions that are called by the program:
      - a. main(): calls two other functions: get_requirements() and estimate_painting_cost()
      - b. get_requirements(): displays the program requirements
      - c. estimate_painting_cost(): calculates interior home painting, and calls print function
      - d. print_painting_estimate(): displays painting costs.
      - e. print_painting_percentage(): displays painting costs percentage

 [A4 README.md](A4/README.md "A4 File")

   - Data Analysis 2

    - This Code will upload data from a dataframe and display the data per request of the assignment below:

    - Program Requirements:
    -  Run demo.py
    -  If errors, more than likely missing installations
    -  Test Python Package Installer: pip freeze
    -  Research how to do the following installations:
     -  a. pandas (only if missing)
     -  b. pandas-datareader (only if missing)
     -  c. matplotlib (only if missing)
    -  Create at least three functions that are called by the program:
     -  a. main(): calls two other functions:
     -  b. get_requirements(): displays the program requirements
     -  c. data_analysis_2(): displays the following data

 [A5 README.md](A5/README.md "A5 File")
 
   - Introduction to R, Setup and Tutorial

   - This Code will upload data from a dataframe and display the data per request of the assignment below:
   
   - Program Requirements:
   - R Commands: save a file of all the R commands included in the tutorial.
   - R Console: save a screenshot of some of the R commands executed above (below requirement)
   - Graphs: save at least 5 separate image files displaying graph plots created from the tutorial.
   - RStudio: save one screenshot (similar to the one below), displaying the following 4 windows:
     - a. R sourcecode (top-left corner)
     - b. Console (bottom-left corner)
     - c. Environment (or History), (top-right corner)
     - d. Plots(bottom-rightcorner)  

 [P1 README.md](P1/README.md "P1 File")

   - Data Analysis

    - This Code will upload data from a dataframe and display the data per request of the assignment below:

    - Program Requirements:
    -  Run demo.py
    -  If errors, more than likely missing installations
    -  Test Python Package Installer: pip freeze
    -  Research how to do the following installations:
     -  a. pandas (only if missing)
     -  b. pandas-datareader (only if missing)
     -  c. matplotlib (only if missing)
    -  Create at least three functions that are called by the program:
     -  a. main(): calls two other functions:
     -  b. get_requirements(): displays the program requirements
     -  c. data_analysis_1(): displays the following data

 [P2 README.md](P2/README.md "P2 File")

   - Project #2

   - This Code will upload data from a dataframe using RStudio and display the data per request of the assignment below:
   
   - Program Requirements:
   - Use Assignment 5 screenshots and R Manual to backward-engineer the following requirements:
   - Resources:a. R Manual: https://cran.r-project.org/doc/manuals/r-release/R-lang.pdfb.
   - R for Data Science: https://r4ds.had.co.nz/
   - Use Motor Trend Car Road Tests data:
     - a. Research the data! https://stat.ethz.ch/R-manual/R-devel/library/datasets/html/mtcars.html
     - b. url = "http://vincentarelbundock.github.io/Rdatasets/csv/datasets/mtcars.csv"
   - Note: Use variable "mtcars" to read file into. 
   - (See Assignment 5 for reading .csv files.)