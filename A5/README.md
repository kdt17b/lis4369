#LIS 4369
> **NOTE:** This README.md file should be placed at the **root of each of your repos directories.**
>
>Also, this file **must** use Markdown syntax, and provide project documentation as per below--otherwise, points **will** be deducted.
>

# LIS4369 - Extensible Enterprise Solutions

## Kenneth D Turner II

### Assignment 5 Requirements:

*Four Parts:*

    Part One:
    
    Assignment 5

    1. Requirements: 
        a. Complete the following tutorial: Introduction_to_R_Setup_and_Tutorial
        b. Code and run lis4369_a5.R
        c. Be sure to include at least two plots in your README.md file
    2. Be sure to test your program using RSTUDIO.    

#### README.md file should include the following items:

* Assignment requirements, as per A1

#### Assignment Screenshots:

*Screenshot of Commands used in Rstudio Tutorial

![Screenshot of Commands](img/CommandList1.png)
![Screenshot of Commands](img/Command_List2.png)

*Screenshot of Executed R commands in Rstudio

![Screenshot of Executed R commands](img/Executed_R_Commands.png)


*Screenshot of 5 separate image files displaying graph plots created from the tutorial

![Graphs](img/Rplot01.png)
![Graphs](img/Rplot.png)
![Graphs](img/Rplot02.png)
![Graphs](img/Rplot05.png)
![Graphs](img/Rplot03.png)


*Screenshot displaying the 4 windows of RStudio:

![Screenshot of 4 windows of RStudio](img/rstudio_console.png)


*Four Parts:*

    Part Two:
    
    Assignment 5

    1. Assignment requirements, as per A1
    2. Screenshots of output from code below

![Screenshot of Executed lis4369_a5.R code](img/lis4369_a5_1.png)
![Screenshot of Executed lis4369_a5.R code](img/lis4369_a5_2.png)
![Screenshot of Executed lis4369_a5.R code](img/lis4369_a5_3.png)
![Screenshot of Executed lis4369_a5.R code](img/lis4369_a5_4.png)
![Screenshot of Executed lis4369_a5.R code](img/lis4369_a5_5.png)
![Screenshot of Executed lis4369_a5.R code](img/lis4369_a5_6.png)
![Screenshot of Executed lis4369_a5.R code](img/lis4369_a5_7.png)
![Screenshot of Executed lis4369_a5.R code](img/lis4369_a5_8.png)
![Screenshot of Executed lis4369_a5.R code](img/lis4369_a5_9.png)
![Screenshot of Executed lis4369_a5.R code](img/lis4369_a5_10.png)
![Screenshot of Executed lis4369_a5.R code](img/lis4369_a5_11.png)
![Screenshot of Executed lis4369_a5.R code](img/lis4369_a5_12.png)
![Screenshot of Executed lis4369_a5.R code](img/lis4369_a5_13.png)
![Screenshot of Executed lis4369_a5.R code](img/lis4369_a5_14.png)


*Screenshot of 2 separate image files displaying graph plots from lis4369_a5.R

![Graphs](img/Rplot080.png)
![Graphs](img/Rplot010.png)

#### Tutorial Links:

*Bitbucket Tutorial - Station Locations:*
[A1 Bitbucket Station Locations Tutorial Link](https://bitbucket.org/kdt17b/bitbucketstationlocations/ "Bitbucket Station Locations")

*Tutorial: Request to update a teammate's repository:*
[A1 My Team Quotes Tutorial Link](https://bitbucket.org/kdt17b/myteamquotes/ "My Team Quotes Tutorial")
