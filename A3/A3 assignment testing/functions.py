def get_requirements():

    print("Payroll Calculator")

    print ("\nProgram Requirements: \n"
    +"1. Must Use float data type for user input. \n"
    +"2. Overtime rate: 1.5 times hourly rate (hours over 40).\n"
    +"3. Holiday rate: 2.0 times hourly rate (all holiday hours)\n"
    +"4. Must format currency with dollar sign, and round to two decimal places. \n"
    +"5. Create at least three functions that are called by the program: \n"
    +"\t a. main(): calls at least two other functions.\n "
    +"\t b. get_requirements(): displays the program requirements\n "
    +"\t c. calculate_payroll(): calculates an individual one-week paycheck")




def calculate_payroll():
    # constants to represent base hours, overtime and holiday rates
    hours = round(40,2)

    print("Input\n")
    hours = (float(input("Enter hours worked: ", )))

    holiday = (float(input("Enter holiday hours: ", )))

    payrate =(float(input("Enter hourly pay rate: ", )))

 
    base = round(hours * payrate, 2)

    overtimehours = round(hours - 40.00,2)

    overtimerate = round(payrate * 1.5, 2)

    holidayrate = round(payrate * 10 * 2.0, 2 )

    overtimepay = round(overtimehours * overtimerate)

    grosspay = round(base+overtimepay+holidayrate,2)

    if hours <= 40:

     print("\nOutput:\n")   
     print("Base:\t\t$" "%.2f" % base)

     print("Overtime:\t  $0.00")

     print("Holiday Pay:\t$" "%.2f" % holidayrate)

     print("Gross:\t\t$" "%.2f" % grosspay)

    elif hours > 40:

        hours = 40

        base = round(hours * payrate, 2)

        grosspay = round(base+overtimepay+holidayrate,2)

        print("\nOutput:\n") 
        print("Base:\t\t$" "%.2f" % base)

        print("Overtime:\t$" "%.2f" % overtimepay)

        print("Holiday Pay:\t$" "%.2f" % holidayrate)

        print("Gross:\t\t$" "%.2f" % grosspay)