

def get_requirements():

    print("Painting Estimator")

    print ("\nProgram Requirements: \n"
    +"1. Calculate home interior paint cost (w/o primer). \n"
    +"2. Must use float data types.\n"
    +"3. Must use SQFT_PER_GALLON constant (350).\n"
    +"4. Must use iteration structure (aka ""loop"") \n"
    +"5. Create at least five functions that are called by the program: \n"
    +"\t a. main(): calls two other functions: get_requirements() and estimate_painting_cost().\n "
    +"\t b. get_requirements(): displays the program requirements\n "
    +"\t c. estimate_painting_cost(): calculates interior home painting, and calls print function\n"
    +"\t d. print_painting_estimate(): displays painting costs.\n"
    +"\t e. print_painting_percentage(): displays painting costs percentage\n")

def estimate_painting_cost():

    total_interior = 0.0
    price_per_gallon = 0.0
    hourly_painting_rate = 0.0
    sq_ft_per_gallon = 350
    

    print("\nInput:")

    total_interior = float(input("Enter total interior sq ft:"))

    price_per_gallon = float(input("Enter price per gallon paint:"))

    hourly_painting_rate = float(input("Enter hourly painting rate per sq ft:"))

    
    
    number_gallons = total_interior / sq_ft_per_gallon

    paint_cost = price_per_gallon * number_gallons
    labor_cost = total_interior * hourly_painting_rate
    total_cost = paint_cost + labor_cost

    paint_percentage = paint_cost / total_cost * 100
    labor_percentage = labor_cost / total_cost * 100
    total_percentage = total_cost / total_cost * 100

    print_painting_estimate(total_interior,sq_ft_per_gallon,number_gallons,price_per_gallon,hourly_painting_rate)
    print_painting_percentage(paint_cost,paint_percentage,labor_cost,labor_percentage,total_cost,total_percentage)

    selection = ['y','n']
    selection = str(input("\nEstimate another paint job? (y/n)"))
    if selection == 'y':
        estimate_painting_cost()
    else:
        print("\nThank you for using our Painting Estimator!")
        print("Please see our web site: http://www.mysite.com\n")

def print_painting_estimate(total_interior,sq_ft_per_gallon,number_gallons,price_per_gallon,hourly_painting_rate):
    print("Output")

    print ("{0:8} {1:>15}".format ("Item","Amount"))
    print ("{0:12} {1:11,.2f}".format ("Total Sq Ft:",total_interior))
    print ("{0:12} {1:5,.2f}".format ("Sq Ft per Gallon:",sq_ft_per_gallon))
    print ("{0:12} {1:5,.2f}".format ("Number of Gallons:",number_gallons))
    print ("{0:12} ${1:5,.2f}".format ("Paint per Gallon:",price_per_gallon))
    print ("{0:12} ${1:>6,.2f}".format ("Labor per Sq Ft:",hourly_painting_rate))
    print ("\n")

def print_painting_percentage(paint_cost,paint_percentage,labor_cost,labor_percentage,total_cost,total_percentage):
    print ("{0:8} {1:>11} {2:>11}".format ("Cost","Amount","Percentage"))
    print ("{0:12} ${1:>4.2f} {2:>10.2f}%".format ("Paint:",paint_cost,paint_percentage))
    print ("{0:12} ${1:>4.2f} {2:>10.2f}%".format ("Labor:",labor_cost,labor_percentage))
    print ("{0:12} ${1:>4.2f} {2:>10.2f}%".format ("Total:",total_cost,total_percentage))

    
    
    





