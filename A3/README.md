#LIS 4369
> **NOTE:** This README.md file should be placed at the **root of each of your repos directories.**
>
>Also, this file **must** use Markdown syntax, and provide project documentation as per below--otherwise, points **will** be deducted.
>

# LIS4369 - Extensible Enterprise Solutions

## Kenneth D Turner II

### Assignment 3 Requirements:

*Five Parts:*

    Painting Estimator

    1. Calculate home interior paint cost (w/o primer).
    2. Must use float data types.
    3. Must use SQFT_PER_GALLON constant (350).
    4. Must use iteration structure (aka ""loop"") 
    5. Create at least five functions that are called by the program:
     a. main(): calls two other functions: get_requirements() and estimate_painting_cost().
     b. get_requirements(): displays the program requirements
     c. estimate_painting_cost(): calculates interior home painting
     d. print_painting_estimate(): displays painting costs.
     e. print_painting_percentage(): displays painting costs percentage

#### README.md file should include the following items:

* Assignment requirements, as per A3
* Screenshot of application running

#### Assignment Screenshots:

*Screenshot of Painting Estimator application running (IDLE)

![IDLE Screenshot](img/Painting_Estimator_IDLE.png)


*Screenshot of Painting Estimator running (Visual Studio Code)

![Visual Studio Code](img/Painting_Estimator.png)

#### Tutorial Links:

*Bitbucket Tutorial - Station Locations:*
[A1 Bitbucket Station Locations Tutorial Link](https://bitbucket.org/kdt17b/bitbucketstationlocations/ "Bitbucket Station Locations")

*Tutorial: Request to update a teammate's repository:*
[A1 My Team Quotes Tutorial Link](https://bitbucket.org/kdt17b/myteamquotes/ "My Team Quotes Tutorial")
