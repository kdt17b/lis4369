#LIS 4369
> **NOTE:** This README.md file should be placed at the **root of each of your repos directories.**
>
>Also, this file **must** use Markdown syntax, and provide project documentation as per below--otherwise, points **will** be deducted.
>

# LIS4369 - Extensible Enterprise Solutions

## Kenneth D Turner II

### Assignment 2 Requirements:

*Three Parts:*

1. Backward-engineer (using Python) the following screenshots:
2. The program should be organized with two modules (See Ch. 4):
    a) functions.py module contains the following functions: 
        i) get_requirements()
        ii) calculate_payroll()
        iii) print_pay
    b) main.py module imports the functions.py module, and calls the functions
3. Be sure to test your program using both IDLE and Visual Studio Code.

#### README.md file should include the following items:

* Assignment requirements, as per A1
* Screenshot of application running

#### Assignment Screenshots:

*Screenshot of Payroll Calculator application running (IDLE)

![IDLE Screenshot](img/Payroll_Calculator_IDLE.png) *no overtime
![IDLE Screenshot](img/Payroll_Calculator_IDLE_Overtime.png) *overtime

*Screenshot of ra1_tip_calculator application running (Visual Studio Code)

![Visual Studio Code](img/Payroll_Calculator.png) *no overtime
![Visual Studio Code](img/Payroll_Calculator_Overtime.png) *overtime

#### Tutorial Links:

*Bitbucket Tutorial - Station Locations:*
[A1 Bitbucket Station Locations Tutorial Link](https://bitbucket.org/kdt17b/bitbucketstationlocations/ "Bitbucket Station Locations")

*Tutorial: Request to update a teammate's repository:*
[A1 My Team Quotes Tutorial Link](https://bitbucket.org/kdt17b/myteamquotes/ "My Team Quotes Tutorial")
