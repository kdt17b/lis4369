

def get_requirements():

    print("Data Analysis 1")

    print ("\nProgram Requirements: \n"
    +"1. Run demo.py. \n"
    +"2. If errors, more than likely missing installations.\n"
    +"3. Test Python Package Installer: pip freeze\n"
    +"4. Research how to do the following installations: \n"
    +"\t a. pandas (only if missing)\n "
    +"\t b. pandas-datareader (only if missing)\n "
    +"\t c. matplotlib (only if missing)\n"
    +"5. Create at least three functions that are called by the program: \n"
    +"\t a. main(): calls at least two other functions\n"
    +"\t b. get_requirements(): displays the program requirements\n"
    +"\t c. data_analysis_1(): displays the following data. \n")



def data_analysis_1():
    import pandas as pd
    import datetime
    import pandas_datareader as pdr
    import matplotlib.pyplot as plt
    from matplotlib import style

    start = datetime.datetime(2010,1,1)
    end = datetime.datetime(2019,10,19)

    df = pdr.DataReader("XOM","yahoo",start,end)

    print("\nPrint number of records: ")
    print(len(df))
    print("\n")

    print(df.columns)

    print("\nPrint data frame: ")
    
    #pd.set_option('display.max_rows', len(df))
    #print(df.iloc[:60])
    print(df)

    print("\nPrint first five lines:")
    print(df.head())

    print("\nPrint last five lines:")
    print(df.tail())

    print("\nPrint first 2 lines:")
    print(df.head(2))

    print("\nPrint last 2 lines:")
    print(df.tail(2))

    style.use('ggplot')

    df['High'].plot()
    df['Adj Close'].plot()
    plt.legend()
    plt.show()